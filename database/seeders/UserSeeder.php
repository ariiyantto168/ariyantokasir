<?php

namespace Database\Seeders;

use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    public function run()
    {
        $dbusers = [
            [
               'username' => 'admin',
               'email' => 'admin@gmail.com',
               'is_active' => true,
               'role' => 'admin',
               'slug' => Uuid::uuid4()->toString(),
               'password' =>  Hash::make('admin123'),
            ],
            [
                'username' => 'kasir',
                'email' => 'kasir@gmail.com',
                'is_active' => true,
                'role' => 'kasir',
                'slug' => Uuid::uuid4()->toString(),
                'password' =>  Hash::make('kasir123'),
            ],
       ];

       DB::table('users')->insert($dbusers);
    }
}
