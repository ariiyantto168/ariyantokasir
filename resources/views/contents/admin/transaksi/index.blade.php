<link rel="stylesheet" type="text/css" href="/assets/css/vendors/datatables.css">
<script src="/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/js/datatable/datatables/datatable.custom.js"></script>
<div class="page-body">
     <!-- open head-->
     <div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Data Transaksi </h5>
                    <a href="{{ url('transaksi') }}" class="btn btn-primary"
                    data-bs-toggle="modal" data-original-title="test" data-bs-target="#tambah_modal">Tambah</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="stripe" id="example-style-8">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>PIC Kasir</th>
                                    <th>Number Table</th>
                                    <th>code</th>
                                    <th>Total</th>
                                    <th>Payment</th>
                                    <th>Status</th>
                                    <th>Money</th>
                                    <th>Change</th>
                                    <th>Jumlah </th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($transaksi as $idx => $transaksi)
                              <tr>
                                    <td>{{$idx+1}}</td>
                                    <td>{{$transaksi->users->username}}</td>
                                    <td>{{$transaksi->number_table}}</td>
                                    <td>{{$transaksi->code}}</td>
                                    <td>{{$transaksi->total}}</td>
                                    <td>{{$transaksi->payment}}</td>
                                    <td>{{$transaksi->status}}</td>
                                    <td>{{$transaksi->money}}</td>
                                    <td>{{$transaksi->change}}</td>
                                    <td>{{count($transaksi->transactiondetail)}}</td>
                                    <td>
                                        <a class="btn btn-warning btn-pill m-1" href="{{('transaksi/'.$transaksi->slug)}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                              </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

     <!-- close head -->
</div>




<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
                    <h5> Transaksi </h5>

                    <div class="modal fade" id="tambah_modal" tabindex="-1" role="dialog" aria-labelledby="tambah_modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <form class="modal-content" method="post" action="{{ url('transaksi') }}" enctype="multipart/form-data">
                            @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="tambah_modalLabel">Form Transaksi</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body py-4 o-hidden">
                                    <div class="form-group">
                                        <label class="col-form-label">Code PO<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="text" value="AUTO" readonly />
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Table <span class="text-danger"><sup>*</sup></span></label>
                                        <select class="form-select digits" name="number_table">
                                            <option value=""> -- select table -- </option>
                                                @foreach($table as $table)
                                                    <option value="{{$table->number_table}}">{{$table->number_table}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name_materi" class="col-sm-2 col-form-label">Payment</label>
                                            <select name="payment" id="select2" class="form-control" required>
                                                <option value="bayar">Bayar</option>
                                                <option value="belumbayar">Belum Bayar</option>
                                            </select>        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">Total<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="number" id="totalall"  name="totalall" value="0" readonly />
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Money<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="number" id="money"  name="money" value="0"  />
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Change<span class="text-danger"><sup>*</sup></span></label>
                                        <!-- <input class="form-control" type="number" id="change"  name="change" value="0" readonly onkeyup="change()"/> -->
                                        <input class="form-control" type="number" id="change"  name="change" value="0" readonly/>
                                    </div>
                                    <br>
                                    <span class="pull-right btn-sm btn-primary" id="addtransaksi">add</span>
                                    
                                    <br>
                                    <table id="table" class="card-body">
                                           <tr>

                                                    <td class="col-md-1 mb-1">
                                                        <label for="validationTooltip01">1.</label>
                                                    </td>
                                                    <td class="col-md-3 mb-2">
                                                        <label for="validationTooltip01">Foods</label>
                                                        <select class="form-select digits" name="id_foods[]" id="id_foods_1" onchange="passing_price(1,this.value);">
                                                            <option value=""> -- select foods -- </option>
                                                            @php
                                                                $param = [];
                                                            @endphp
                                                            @foreach($foods as $food)
                                                            @php
                                                                $param[$food->id_foods] = $food->price;    
                                                            @endphp
                                                            <option value="{{$food->id_foods}}">{{$food->namefoods}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td class="col-md-3 mb-2">
                                                        <label for="validationTooltip02">Quantity</label>
                                                        <input class="form-control" name="quantity[]" id="quantity_1" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value(1)" >
                                                    </td>
                                                    <td class="col-md-3 mb-2">
                                                        <label for="validationTooltip02">Price</label>
                                                        <input class="form-control" name="price[]" id="price_1" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value(1)">
                                                    </td>
                                                    <td class="col-md-3 mb-2">
                                                        <label for="validationTooltip02">Total</label>
                                                        <input class="form-control" name="totalsendiri[]" id="total_1" type="number" value="0" data-bs-original-title="" title="" onkeyup="total(1)">
                                                    </td>
                                            </tr>
                                       
                                    </table>
                                    <br>
                                       
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-success" type="button" onclick="hitung()">Hitung</button>
                                    <button class="btn btn-secondary" type="submit"  id="btn_save">Submit</button>
                                </div>
                                <input type="hidden" value="2" id="appendindex">
                            </form>
                        </div>
                    </div>
        </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    var makan = {!!json_encode($param)!!};
    console.log(makan);
    var id_foods = '';
    @foreach($foods as $food)
      id_foods += "<option value='{{$food->id_foods}}'>{{$food->namefoods}}</option>";
    @endforeach

    // remove table add
    // delete row
    $('#table').on('click', '.del' ,function() {
      $(this).closest('tr').remove();
    });

    $('#addtransaksi').on('click', function(){
        var ais = $('#appendindex').val();
        // console.log(ais)
        $('#appendindex').val(parseInt(ais)+1);

        $('#table').append('<tr>'
                    +'<td class="col-md-1 mb-1">'
                        +'<label for="validationTooltip01">'+ais+'</label>'
                        +'<a class="btn btn-xs del"><i class="fa fa-trash" aria-hidden="true"></i></a>'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Foods</label>'
                        +'<select class="form-select digits" onchange="passing_price('+ais+',this.value)" name="id_foods[]" id="id_foods_'+ais+'"> <option>- select foods -</option>'+id_foods+
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Quantity</label>'
                        +'<input class="form-control" name="quantity[]" id="quantity_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value('+ais+')">'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Price</label>'
                        +'<input class="form-control" name="price[]" id="price_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value('+ais+')">'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Total</label>'
                        +'<input class="form-control" name="totalsendiri[]" id="total_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="total('+ais+')">'
                    +'</td>'
        +'</tr>'
        );
    })

    // fungsi
    function passing_price(id,val){
      $('#price_'+id).val(makan[val]);
      count_value(id);
      total();
      change();
    }

    function count_value(id){
      var qty = $('#quantity_'+id).val() || 0; 
      var price = $('#price_'+id).val() || 0;
      setTimeout(function(){
        var total = parseInt(qty) * parseInt(price);
        if(total > 0){
          $('#total_'+id).val(total);
        }else{
          $('#total_'+id).val(0);
        }
      }, 500);
      // console.log(y); 
      total();
      change();

    }

    function total(){
      var count_all = 0;
      var totalall = $('#appendindex').val();
    //   var money = $('#money').val();
      

      setTimeout(function(){
        for (var i = 1; i < totalall; i++) {
          var total = $('#total_'+i).val();
          if (typeof total !== 'undefined') {
            count_all += parseInt(total);
          }
          $('#totalall').val(count_all);
          var money = $('#money').val();
          change();
        
          
        }
      },500)
    }

    function change(){
      var money = $('#money').val();
      var totalall = $('#totalall').val();
     
    
      setTimeout(function(){
        var change = parseInt(money) - parseInt(totalall);
      $('#change').val(change);   
      },500)

    }
    function hitung(){
      change();

      $('#btn_save').prop('disabled',false);
    }
</script>


