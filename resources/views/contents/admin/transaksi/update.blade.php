<link rel="stylesheet" type="text/css" href="/assets/css/vendors/datatables.css">
<script src="/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/js/datatable/datatables/datatable.custom.js"></script>
<script src="../assets/js/datepicker/date-time-picker/moment.min.js"></script>
    <script src="../assets/js/datepicker/date-time-picker/tempusdominus-bootstrap-4.min.js"></script>
    <script src="../assets/js/datepicker/date-time-picker/datetimepicker.custom.js"></script>
<div class="page-body">
     <!-- open head-->
     <div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Edit Transaksi </h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                       <!-- form input -->
                       <form class="theme-form" role="form" enctype="multipart/form-data" method="post" action="{{ url('transaksi/'.$transaksi->slug) }}">
                        @csrf
                            <div class="modal-body py-4 o-hidden">
                            <input type="hidden" value="{{$transaksi->id_transaction}}" name="id_transaction">
                                <div class="form-group">
                                    <label class="col-form-label">Code PO<span class="text-danger"><sup>*</sup></span></label>
                                    <input class="form-control" type="text" value="{{$transaksi->code}}" readonly />
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Table <span class="text-danger"><sup>*</sup></span></label>
                                    <select class="form-select digits" name="number_table">
                                        @foreach($table as $table)
                                            <option value="{{$table->number_table}}" @if ($table->number_table == $transaksi->number_table) 
                                                selected
                                            @endif>{{$table->number_table}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="name_materi" class="col-sm-2 col-form-label">Payment</label>
                                            <select name="payment" id="select2" class="form-control" required>
                                                <option value="bayar" @if($transaksi->payment == 'bayar')
                                                  selected
                                                @endif>Bayar</option>
                                                <option value="belumbayar" @if($transaksi->payment == 'belumbayar')
                                                  selected
                                                @endif>Belum Bayar</option>

                                            </select>        
                                </div>
                                <div class="form-group">
                                        <label for="name_materi" class="col-sm-2 col-form-label">Status</label>
                                            <select name="status" id="select2" class="form-control" required>
                                                <option value="antrian" @if($transaksi->status == 'antrian')
                                                  selected
                                                @endif>Antrian</option>
                                                <option value="memasak" @if($transaksi->status == 'memasak')
                                                  selected
                                                @endif>Memasak</option>
                                                <option value="ready" @if($transaksi->ready == 'ready')
                                                  selected
                                                @endif>Ready</option>
                                            </select>        
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Total<span class="text-danger"><sup>*</sup></span></label>
                                    <input class="form-control" type="number" id="totalall"  name="totalall" value="{{$transaksi->total}}" readonly />
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Money<span class="text-danger"><sup>*</sup></span></label>
                                    <input class="form-control" type="number" id="money"  name="money" value="{{$transaksi->money}}"  />
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Change<span class="text-danger"><sup>*</sup></span></label>
                                    <input class="form-control" type="number" id="change"  name="change" value="{{$transaksi->change}}" readonly/>
                                </div>
                                <br>
                                <span class="pull-right btn-sm btn-primary" id="addtransaksi">add</span>
                                <br>
                                <table id="table" class="card-body">
                                    @foreach($transaksi->transactiondetail as $index => $trs)
                                    <tr>
                                        <td class="col-md-1 mb-1">
                                            <label for="validationTooltip01">{{$index+1}}</label>
                                            <a class="btn btn-xs"><i class="fa fa-trash" value="{{$trs->id_transaction_detail}}" aria-hidden="true" data-bs-toggle="modal" data-original-title="test" data-bs-target="#delete_detail"></i></a>'
                                        </td>
                                        <input type="hidden" value="{{$trs->id_transaction_detail}}" id="id_transaction_detail_{{$index+1}}" name="id_transaction_detail[]">
                                        <td class="col-md-3 mb-2">
                                            <label for="validationTooltip01">Foods</label>
                                            <select class="form-select digits" name="id_foods[]"  id="id_foods_{{$index+1}}" onchange="passing_price({{$index+1}},this.value);">>
                                                <option value=""> -- select foods -- </option>
                                                    @php
                                                        $param = [];
                                                    @endphp
                                                    @foreach($foods as $food)
                                                        @php
                                                            $param[$food->id_foods] = $food->price;    
                                                        @endphp
                                                        <option value="{{$food->id_foods}}" @if ($food->id_foods == $trs->id_foods)
                                                            selected
                                                        @endif>{{$food->namefoods}}</option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td class="col-md-3 mb-2">
                                            <label for="validationTooltip02">Quantity</label>
                                            <input class="form-control" name="quantity[]" id="quantity_{{$index+1}}" type="number" value="{{$trs->quantity}}" data-bs-original-title="" title="" onkeyup="count_value(1)" >
                                        </td>
                                        <td class="col-md-3 mb-2">
                                            <label for="validationTooltip02">Price</label>
                                            <input class="form-control" name="price[]"  type="number" value="{{$trs->foods->price}}" data-bs-original-title="" title="" id="price_{{$index+1}}" onkeyup="count_value(1)">
                                        </td>
                                        <td class="col-md-3 mb-2">
                                            <label for="validationTooltip02">Total</label>
                                            <input class="form-control" name="totalsendiri[]" id="total_{{$index+1}}"  type="number" value="{{$trs->total}}" data-bs-original-title="" title="" onkeyup="total(1)">
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                                <br>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-success" type="button" onclick="hitung()">Hitung</button>
                                    <button class="btn btn-secondary" type="submit"  id="btn_save">Submit</button>
                                </div>
                                <input type="hidden" id="appendindex" value="{{$transaksi->transactiondetail->count()+1}}">
                            </div>
					    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

     <!-- close head -->
</div>

<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
                    <h5> Transaksi </h5>

                    <div class="modal fade" id="delete_detail" tabindex="-1" role="dialog" aria-labelledby="tambah_modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <form class="modal-content" method="post" action="{{ url('transaksi-delete') }}" enctype="multipart/form-data">
                            @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="tambah_modalLabel">Delete Transaksi</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                
                                       
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-secondary" type="submit" >Delete</button>
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
var makan = {!!json_encode($param)!!};
    console.log(makan);
    var id_foods = '';
    @foreach($foods as $food)
      id_foods += "<option value='{{$food->id_foods}}'>{{$food->namefoods}}</option>";
    @endforeach

    // remove table add
    // delete row
    $('#table').on('click', '.del' ,function() {
      $(this).closest('tr').remove();
    });

    $('#addtransaksi').on('click', function(){
        var ais = $('#appendindex').val();
        // console.log(ais)
        $('#appendindex').val(parseInt(ais)+1);

        $('#table').append('<tr>'
                    +'<td class="col-md-1 mb-1">'
                        +'<label for="validationTooltip01">'+ais+'</label>'
                        +'<input type="hidden" value="new" name="id_transaction_detail[]" id="id_transaction_detail">'
                        +'<a class="btn btn-xs del"><i class="fa fa-trash" aria-hidden="true"></i></a>'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Foods</label>'
                        +'<select class="form-select digits" onchange="passing_price('+ais+',this.value)" name="id_foods[]" id="id_foods_'+ais+'"> <option>- select foods -</option>'+id_foods+
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Quantity</label>'
                        +'<input class="form-control" name="quantity[]" id="quantity_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value('+ais+')">'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Price</label>'
                        +'<input class="form-control" name="price[]" id="price_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="count_value('+ais+')">'
                    +'</td>'
                    +'<td class="col-md-3 mb-2">'
                        +'<label for="validationTooltip02">Total</label>'
                        +'<input class="form-control" name="totalsendiri[]" id="total_'+ais+'" type="number" value="0" data-bs-original-title="" title="" onkeyup="total('+ais+')">'
                    +'</td>'
        +'</tr>'
        );
    })


function passing_price(id,val){
  $('#price_'+id).val(makan[val]);
  count_value(id);
  total();
  change();
}

function count_value(id){
  var qty = $('#quantity_'+id).val() || 0; 
  var price = $('#price_'+id).val() || 0;
  setTimeout(function(){
    var total = parseInt(qty) * parseInt(price);
    if(total > 0){
      $('#total_'+id).val(total);
    }else{
      $('#total_'+id).val(0);
    }
  }, 500);
  // console.log(y); 
  total();
  change();

}

function total(){
  var count_all = 0;
  var totalall = $('#appendindex').val();
  var money = $('#money').val();

  setTimeout(function(){
    for (var i = 1; i < totalall; i++) {
      var total = $('#total_'+i).val();
      if (typeof total !== 'undefined') {
        count_all += parseInt(total);
      }
      $('#totalall').val(count_all);
      change();
      
    }
  },500)
}

function change(){
  var money = $('#money').val();
  var totalall = $('#totalall').val();
  // console.log(money);
 
  setTimeout(function(){
    var change = parseInt(money) - parseInt(totalall);
  $('#change').val(change);   
  },500)

}
function hitung(){
  change();

  $('#btn_save').prop('disabled',false);
}
</script>
