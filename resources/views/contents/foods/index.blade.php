<link rel="stylesheet" type="text/css" href="/assets/css/vendors/datatables.css">
<script src="/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/js/datatable/datatables/datatable.custom.js"></script>
<div class="page-body">
     <!-- open head-->
     <div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Data foods </h5>
                    <a href="{{ url('foods') }}" class="btn btn-primary"
                    data-bs-toggle="modal" data-original-title="test" data-bs-target="#tambah_modal">Tambah</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="stripe" id="example-style-8">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama foods</th>
                                    <th>Price</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($foods as $idx => $food)
                                <tr>
                                    <td>{{$idx+1}}</td>
                                    <td>{{$food->namefoods}}</td>
                                    <td>{{$food->price}}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

     <!-- close head -->
</div>


<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
                    <h5> Category </h5>

                    <div class="modal fade" id="tambah_modal" tabindex="-1" role="dialog" aria-labelledby="tambah_modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <form class="modal-content" method="post" action="{{ url('datamaster/foods') }}" enctype="multipart/form-data">
                            @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="tambah_modalLabel">Form Artikel</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body py-4 o-hidden">
                                    <div class="form-group">
                                        <label class="col-form-label">Name foods<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="text"  name="namefoods" required  placeholder="Example: Dimsum seafoods" />
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Price foods<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="number"  name="price" required  placeholder="Example: 25000" />
                                    </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-secondary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
    </div>
</div>

