<link rel="stylesheet" type="text/css" href="/assets/css/vendors/datatables.css">
<script src="/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/js/datatable/datatables/datatable.custom.js"></script>
<div class="page-body">
     <!-- open head-->
     <div class="container-fluid">
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Activity users </h5>
                    
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="stripe" id="example-style-8">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>username</th>
                                    <th>Aktifitas</th>
                                    <th>Akses data</th>
                                    <th>Akses Time</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($activity as $idx => $act)
                                <tr>
                                    <td>{{$idx+1}}</td>
                                    <td>{{$act->users->username}}</td>
                                    <td>{{$act->media_activity}}</td>
                                    @if(!empty($act->slugfoods))
                                        <td>{{$act->slugfoods->namefoods}}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td>{{$act->created_at}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

     <!-- close head -->
</div>

