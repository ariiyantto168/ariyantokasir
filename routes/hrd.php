<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Hrd\TrainingController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('training/hrd', [TrainingController::class, 'index'])->middleware('check.auth');
Route::get('training/hrd/{slug}/data', [TrainingController::class, 'data_training'])->middleware('check.auth');
Route::get('training/hrd/{slug}/data/{slugdetail}', [TrainingController::class, 'view_tambah_peserta_admin'])->middleware('check.auth');