<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ActivityController;
use App\Http\Controllers\Datamaster\FoodsController;
use App\Http\Controllers\TransaksiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', [AuthController::class, 'login']);
Route::post('login-do', [AuthController::class, 'do_login']);
Route::get('admin', [HomeController::class, 'admin'])->middleware('check.auth');
// datamaster foods
Route::get('datamaster/foods', [FoodsController::class, 'index'])->middleware('check.auth');
Route::post('datamaster/foods', [FoodsController::class, 'create'])->middleware('check.auth');
// activity
Route::get('activity', [ActivityController::class, 'index']);
// transaksi
Route::get('transaksi', [TransaksiController::class, 'index'])->middleware('check.auth');
Route::post('transaksi', [TransaksiController::class, 'create'])->middleware('check.auth');
Route::get('transaksi/{slug}', [TransaksiController::class, 'get_slug'])->middleware('check.auth');
Route::post('transaksi/{slug}', [TransaksiController::class, 'update_slug'])->middleware('check.auth');
Route::post('transaksi-delete', [TransaksiController::class, 'delete_detail'])->middleware('check.auth');