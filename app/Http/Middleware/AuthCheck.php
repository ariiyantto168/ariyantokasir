<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthCheck
{

    public function handle(Request $request, Closure $next)
    {

        if(!$request->session()->exists('users')) {
            return redirect('/')->with('status_warning', 'Please Login!');
        }
        return $next($request);
    }
}
