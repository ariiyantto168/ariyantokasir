<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function admin()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.admin.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.admin.masterpage', $pagemain);
    }

    public function manager()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.manager.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home_manager',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.manager.masterpage', $pagemain);
    }

    public function hrd()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.hrd.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home_hrd',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.hrd.masterpage', $pagemain);
    }

    public function finance()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.finance.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home_finance',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.finance.masterpage', $pagemain);
    }
}
