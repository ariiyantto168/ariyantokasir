<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Models\User;
use App\Models\Activityusers;
use App\Models\Datamaster\Foods;
use App\Models\Datamaster\Tablecustomer;
use App\Models\Transaksi\Transaction;
use App\Models\Transaksi\Transactiondetail;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {

            $contents = [
                'foods' => Foods::all(),
                'table' => Tablecustomer::all(),
                'transaksi' => Transaction::with(['users','transactiondetail','table'])->get(),
            ];
            // return $contents;
            
            $pagecontent = view('contents.admin.transaksi.index', $contents);
    
            //masterpage
            $pagemain = array(
                'title' => 'foods transaksi',
                'menu' => 'transaksi',
                'submenu' => 'transaksi',
                'pagecontent' => $pagecontent,
            );
    
    
            return view('contents.admin.masterpage', $pagemain);
        
        

    }

    public function create(Request $request)
    {
        // return $request->all();
        
            $quantity = $request->quantity;
            $totalsendiri = $request->totalsendiri;
            $foods = $request->id_foods;
            $fds = count($foods);

            for ($i=0; $i < $fds; $i++) {
                if($foods[$i] == 0) {
                    return redirect()->back()->with('status_eror', 'Foods empty');
                }elseif ($quantity[$i] == 0){
                    return redirect()->back()->with('status_eror', 'Quantity empty');
                }elseif ($totalsendiri[$i] == 0){
                    return redirect()->back()->with('status_eror', 'Total empty');
                }
            }

            $transaksi = new Transaction;
            $transaksi->id_users = Auth::user()->id_users;
            $transaksi->code = $this->get_code();
            $transaksi->total = $request->totalall;
            $transaksi->payment = $request->payment;
            $transaksi->status = 'antrian';
            $transaksi->money = $request->money;
            $transaksi->change = $request->change;
            $transaksi->number_table = $request->number_table;
            $transaksi->slug = Uuid::uuid4()->toString();
            $transaksi->created_by = Auth::user()->username;
            $transaksi->created_at = date('Y-m-d H:i:s');
            $transaksi->save();

            for ($i=0; $i < $fds; $i++){
                $trsDetail = new Transactiondetail;
                $trsDetail->id_transaction = $transaksi->id_transaction;
                $trsDetail->id_foods = $foods[$i];
                $trsDetail->quantity = $quantity[$i];
                $trsDetail->total = $totalsendiri[$i];
                $trsDetail->created_by = Auth::user()->username;
                $trsDetail->created_at = date('Y-m-d H:i:s');
                $trsDetail->save();
            }

            return redirect('transaksi')->with('status_success','Created Transaksi');
        
        

    }

    public function get_slug($slug)
    {
            $transaksi = Transaction::with(['users',
                                        'transactiondetail' => function($food){
                                            $food->with(['foods']);
                                        },
                                        'table'
                                        ])
                            ->where('slug',$slug)->first();
        
            $contents = [
                'foods' => Foods::all(),
                'table' => Tablecustomer::all(),
                'transaksi' => $transaksi,
            ];
            
            $pagecontent = view('contents.admin.transaksi.update', $contents);

            //masterpage
            $pagemain = array(
                'title' => 'foods transaksi',
                'menu' => 'transaksi',
                'submenu' => 'transaksi',
                'pagecontent' => $pagecontent,
            );


            return view('contents.admin.masterpage', $pagemain);
        
    }

    public function update_slug(Request $request, $slug)
    {
    
            $transaksi_details = $request->id_transaction_detail;
            $quantity = $request->quantity;
            $totalsendiri = $request->totalsendiri;
            $foods = $request->id_foods;
            $fds = count($foods);
    
            for ($i=0; $i < $fds; $i++) {
                if($foods[$i] == 0) {
                    return redirect()->back()->with('status_eror', 'Foods empty');
                }elseif ($quantity[$i] == 0){
                    return redirect()->back()->with('status_eror', 'Quantity empty');
                }elseif ($totalsendiri[$i] == 0){
                    return redirect()->back()->with('status_eror', 'Total empty');
                }
            }
    
            $transaksi = Transaction::where('id_transaction',$request->id_transaction)->first();
            $transaksi->id_users = $transaksi->id_users;
            $transaksi->code = $transaksi->code;
            $transaksi->total = $request->totalall;
            $transaksi->money = $request->money;
            $transaksi->change = $request->change;
            $transaksi->number_table = $request->number_table;
            $transaksi->slug = $transaksi->slug;
            $transaksi->payment = $request->payment;
            $transaksi->status = $request->status;
            $transaksi->updated_by = Auth::user()->username;
            $transaksi->updated_at = date('Y-m-d H:i:s');
            $transaksi->save();
    
            for ($i=0; $i < $fds; $i++){
                if ($transaksi_details[$i] == 'new') {
                    $trsDetail = new Transactiondetail;
                    $trsDetail->id_transaction = $transaksi->id_transaction;
                }else {
                    $trsDetail = Transactiondetail::find($transaksi_details[$i]);
                }
    
    
                $trsDetail->id_foods = $foods[$i];
                $trsDetail->quantity = $quantity[$i];
                $trsDetail->total = $totalsendiri[$i];
                $trsDetail->updated_by = Auth::user()->username;
                $trsDetail->updated_at = date('Y-m-d H:i:s');
                $trsDetail->save();
            }
            return redirect('transaksi')->with('status_success','Created Transaksi');
        


    }

    public function delete_detail(Request $request)
    {
        return $request->all();
    }

    public function get_code()
    {
        $date_ym = date('ymd');
        $date_between = [date('Y-m-01 00:00:00'), date('Y-m-t 23:59:59')];
        
        $dataSellings = Transaction::select('code')
  			->whereBetween('created_at',$date_between)
  			->orderBy('code','desc')
              ->first();
        
              if(is_null($dataSellings)) {
                $nowcode = '00001';
            } else {
                $lastcode = $dataSellings->code;
                $lastcode1 = intval(substr($lastcode, -5))+1;
                $nowcode = str_pad($lastcode1, 5, '0', STR_PAD_LEFT);
            }
  
            return 'PSN-'.$date_ym.'-'.$nowcode;
    }

}
