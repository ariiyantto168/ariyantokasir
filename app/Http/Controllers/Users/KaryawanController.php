<?php

namespace App\Http\Controllers\Users;

use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\Karyawan;
use App\Models\Datamaster\Divisi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KaryawanController extends Controller
{
    public function index_karyawan()
    {
        $user = User::with([
                            'karyawan' => function($divisi){
                                $divisi->with(['divisi']);
                            }
                        ])
                    ->where('role','staff')
                    ->get();

        $contents = [
            'users' => $user
        ];
        // return $contents;

        
        $pagecontent = view('contents.karyawan.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'karyawan',
            'menu' => 'users',
            'submenu' => 'karyawan',
            'pagecontent' => $pagecontent,
        );

        return view('contents.admin.masterpage', $pagemain);
    }

    public function create()
    {
        $divisi = Divisi::all();

        $contents = [
            'divisi' => $divisi
        ];
        
        // return $contents;

        $pagecontent = view('contents.karyawan.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'karyawan',
            'menu' => 'users',
            'submenu' => 'karyawan',
            'pagecontent' => $pagecontent,
        );

        return view('contents.admin.masterpage', $pagemain);
    }
    

    public function save_karyawan(Request $request)
    {
        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = '123456789';
        $user->is_active = true;
        $user->slug = Uuid::uuid4()->toString();
        $user->role = 'staff';
        $user->save();

        $karyawan = new Karyawan;
        $karyawan->id_users = $user->id_users;
        $karyawan->id_divisi = $request->id_divisi;
        $karyawan->first_name = $request->first_name;
        $karyawan->last_name = $request->last_name;
        $karyawan->nama_lengkap = $request->nama_lengkap;
        $karyawan->nomor_nik = $request->nomor_nik;
        $karyawan->nomor_npwp = $request->nomor_npwp;
        $karyawan->idkaryawan = $request->idkaryawan;
        $karyawan->tempat_lahir = $request->tempat_lahir;
        $karyawan->tanggal_lahir = $request->tanggal_lahir;
        $karyawan->tanggal_bergabung = $request->tanggal_bergabung;
        $karyawan->status = $request->status;
        $karyawan->slug = Uuid::uuid4()->toString();
        $karyawan->is_active = true;
        $karyawan->alamat = $request->alamat;
        $karyawan->tinggi = $request->tinggi;
        $karyawan->berat = $request->berat;
        $karyawan->pekerjaan = $request->pekerjaan;
        $karyawan->created_by = Auth::user()->nama;
        $karyawan->created_at = date('Y-m-d H:i:s');
        $karyawan->save();

        return redirect('karyawan')->with('status_success','Successfuly Add Karyawan');

    }

    public function get_karyawan($slug,$slugdetail)
    {
        $user = User::where('slug',$slug)->first();
        if (!empty($user)) {
            $karyawan = Karyawan::where('slug', $slugdetail)->first();
            if (!empty($karyawan)) {
                $data = User::with([
                                    'karyawan' => function($divisi){
                                        $divisi->with(['divisi']);
                                    }
                                ])
                        ->where('slug',$slug)
                        ->first();

                $divisi = Divisi::all();
                
                 $contents = [
                     'divisi' => $divisi,
                     'dataKaryawan' => $data
                 ];


                $pagecontent = view('contents.karyawan.update', $contents);

                //masterpage
                $pagemain = array(
                    'title' => 'karyawan',
                    'menu' => 'users',
                    'submenu' => 'karyawan',
                    'pagecontent' => $pagecontent,
                );

                return view('contents.admin.masterpage', $pagemain);

            }else {
                return redirect('karyawan')->with('status_error','slug tidak tersedia');
            }
        }else {
            return redirect('karyawan')->with('status_error','slug tidak tersedia');
        }
    }


    public function update_karyawan($slug,$slugdetail,Request $request)
    {
        $user = User::where('slug',$slug)->first();
        if (!empty($user)) {
            $karyawan = Karyawan::where('slug', $slugdetail)->first();
            if (!empty($karyawan)) {
                $user->username = $request->username;
                $user->email = $user->email;
                $user->password = '123456789';
                $user->is_active = true;
                $user->slug = $user->slug;
                $user->role = 'staff';
                $user->save();

                $karyawan->id_users = $user->id_users;
                $karyawan->id_divisi = $request->id_divisi;
                $karyawan->first_name = $request->first_name;
                $karyawan->last_name = $request->last_name;
                $karyawan->nama_lengkap = $request->nama_lengkap;
                $karyawan->nomor_nik = $request->nomor_nik;
                $karyawan->nomor_npwp = $request->nomor_npwp;
                $karyawan->idkaryawan = $request->idkaryawan;
                $karyawan->tempat_lahir = $request->tempat_lahir;
                $karyawan->tanggal_lahir = $request->tanggal_lahir;
                $karyawan->tanggal_bergabung = $request->tanggal_bergabung;
                $karyawan->status = $request->status;
                $karyawan->slug = $karyawan->slug;
                $karyawan->is_active = true;
                $karyawan->alamat = $request->alamat;
                $karyawan->tinggi = $request->tinggi;
                $karyawan->berat = $request->berat;
                $karyawan->pekerjaan = $request->pekerjaan;
                $karyawan->updated_by = Auth::user()->nama;
                $karyawan->updated_at = date('Y-m-d H:i:s');
                $karyawan->save();

                return redirect('karyawan')->with('status_success','Successfuly Edit Karyawan');

            }else {
                return redirect('karyawan')->with('status_error','slug tidak tersedia');
            }
        }else {
            return redirect('karyawan')->with('status_error','slug tidak tersedia');
        }
    }



}
