<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Models\User;
use App\Models\Activityusers;
use App\Models\Datamaster\Foods;
use Illuminate\Support\Facades\Auth;


class FoodsController extends Controller
{
    public function index(Request $request)
    {
        $user = User::where('id_users',Auth::user()->id_users)->first();
        
        $contents = [
            'foods' => Foods::all(),
        ];
        // return $contents;
        
        $pagecontent = view('contents.foods.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'foods master',
            'menu' => 'datamaster',
            'submenu' => 'foods',
            'pagecontent' => $pagecontent,
        );

        $activity = new Activityusers;
        $activity->slug_media = $user->slug;
        $activity->created_by = $user->username;
        $activity->created_at = date('Y-m-d H:i:s');
        $activity->media_activity = $user->username .' ' .'akses data foods';
        $activity->save();

        return view('contents.admin.masterpage', $pagemain);

    }

    public function create(Request $request)
    {
        $foods = new Foods;
        $foods->namefoods = $request->namefoods;
        $foods->price = $request->price;
        $foods->slug = Uuid::uuid4()->toString();
        $foods->save();

        $user = User::where('id_users',Auth::user()->id_users)->first();
        $activity = new Activityusers;
        $activity->slug_media = $user->slug;
        $activity->slug_post = $foods->slug;
        $activity->created_by = $user->username;
        $activity->created_at = date('Y-m-d H:i:s');
        $activity->media_activity = $user->username .' ' .'create data foods';
        $activity->save();

        return redirect('datamaster/foods')->with('status_success','Successfuly Add foods');

    }

}
