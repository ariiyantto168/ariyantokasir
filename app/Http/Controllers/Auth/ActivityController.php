<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Models\User;
use App\Models\Activityusers;
use App\Models\Datamaster\Foods;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
        
        $contents = [
            'activity' => Activityusers::with(['users','slugfoods'])->get(),
        ];
        // return $contents;
        
        $pagecontent = view('contents.activity.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'activity user',
            'menu' => 'users',
            'submenu' => 'activity',
            'pagecontent' => $pagecontent,
        );
    

        return view('contents.admin.masterpage', $pagemain);

    }
}
