<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('contents.login.index');
    }

    public function do_login(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('username', 'password');
               
        if (Auth::attempt($credentials)) {
            $datauser = User::where('username',$request->username)->first();
            $datauser->last_login = date('Y-m-d H:i:s');
            $datauser->save();
            // user session 
            $alldata = [
                'idusers' => $datauser->id_users,
                'username' => $datauser->username,
                'email' => $datauser->email,
                'role' => $datauser->role,
            ];
            
            if (!empty($datauser->role === 'admin')) {
                Session::put('users',$alldata);
                return redirect('admin');
            }elseif (!empty($datauser->role === 'manager')) {
                Session::put('users',$alldata);
                return redirect('manager');
            }
        }else{
            return redirect('login')->with('status_warning','email or password something went wrong');
        }


    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect('login');
    }



}
