<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'data_karyawan';
    protected $primaryKey = 'id_data_karyawan';

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'id_users','id_users');
    }

    public function divisi()
    {
        return $this->belongsTo('App\Models\Datamaster\Divisi', 'id_divisi','id_divisi');
    }

}
