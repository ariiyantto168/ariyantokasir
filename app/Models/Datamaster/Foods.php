<?php

namespace App\Models\Datamaster;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foods extends Model
{
    use HasFactory;

    protected $dates = ['deleted_at'];

    protected $table = 'foods';
    protected $primaryKey = 'id_foods';
}
