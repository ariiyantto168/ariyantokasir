<?php

namespace App\Models\Datamaster;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tablecustomer extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];

    protected $table = 'table_customer';
    protected $primaryKey = 'id_table_customer';
}
