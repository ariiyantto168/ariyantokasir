<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activityusers extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'activity_users';
    protected $primaryKey = 'id_activity_users';

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'slug_media','slug');
    }

    public function slugfoods()
    {
        return $this->belongsTo('App\Models\Datamaster\Foods', 'slug_post','slug');
    }

}
