<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactiondetail extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'transaction_detail';
    protected $primaryKey = 'id_transaction_detail';

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaksi\Transaction', 'id_transaction','id_transaction');
    }

    public function foods()
    {
        return $this->belongsTo('App\Models\Datamaster\Foods', 'id_foods','id_foods');
    }
}
