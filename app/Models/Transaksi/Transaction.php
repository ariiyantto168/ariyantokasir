<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'transaction';
    protected $primaryKey = 'id_transaction';

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'id_users','id_users');
    }

    public function table()
    {
        return $this->belongsTo('App\Models\Datamaster\Tablecustomer', 'number_table','number_table');
    }

    public function transactiondetail()
    {
        return $this->hasMany('App\Models\Transaksi\Transactiondetail', 'id_transaction','id_transaction');
    }
}
